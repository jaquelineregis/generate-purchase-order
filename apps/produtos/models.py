from django.db import models
from django.utils.translation import gettext as _


class Produto(models.Model):
    nome = models.CharField(_('Nome'), max_length=50)
    preco = models.DecimalField(_('Preço'), max_digits=10, decimal_places=2)
    multiplo = models.IntegerField(_('Multiplo'), default=1)

    def __str__(self):
        return self.nome
