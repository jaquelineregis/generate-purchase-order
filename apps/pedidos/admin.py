from django.contrib import admin
from .models import Pedido, Item

admin.site.register(Pedido)
admin.site.register(Item)
