from rest_framework import serializers

from apps.pedidos.models import Item, Pedido


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = '__all__'


class PedidoSerializer(serializers.ModelSerializer):
    itens = ItemSerializer(many=True)
    
    class Meta:
        model = Pedido
        fields = '__all__'
