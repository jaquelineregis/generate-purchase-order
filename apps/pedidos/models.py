from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext as _

from apps.clientes.models import Cliente
from apps.produtos.models import Produto


class Item(models.Model):
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE)
    quantidade = models.PositiveIntegerField(_('Quantidade'))
    preco_final = models.FloatField(_('Preço Final'))

    def __str__(self):
        return self.produto.nome


class Pedido(models.Model):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    itens = models.ManyToManyField(Item)
    data = models.DateField(_('Data'), auto_now=True)
    
    def __str__(self):
        return self.cliente.nome
