from django.db import models
from django.utils.translation import gettext as _


class Cliente(models.Model):
    nome = models.CharField(_('Nome'), max_length=50)

    def __str__(self):
        return self.nome
