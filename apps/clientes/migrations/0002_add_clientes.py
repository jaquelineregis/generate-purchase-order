# Generated by Django 2.2.4 on 2019-08-03 21:16

from django.db import migrations


def add_cliente(apps, schema_editor):
    Cliente = apps.get_model('clientes', 'Cliente')

    clientes = [
        {'nome': 'Darth​ ​Vader'},
        {'nome': 'Obi-Wan​ ​Kenobi'},
        {'nome': 'Luke​ ​Skywalker'},
        {'nome': 'Imperador​ ​Palpatine'},
        {'nome': 'Han​ ​Solo'},
    ]
    for cliente in clientes:
        Cliente.objects.create(nome=cliente['nome'])


class Migration(migrations.Migration):

    dependencies = [
        ('clientes', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_cliente),
    ]
