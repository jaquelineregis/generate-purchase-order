# Generate Purchase Order

Python | Django | ReactJS | TDD


Steps:

Open terminal 1 -> your-path/generate-purchase-order

create virtual env (opcional)

pip install -r requirements-dev.txt

python manage.py migrate

python manage.py createsuperuser

python manage.py runserver

Open terminal 2 -> your-path/generate-purchase-order

yarn install or npm install

yarn start or npm start

Open terminal 3 -> your-path/generate-purchase-order

yarn test

or

CI=true yarn test