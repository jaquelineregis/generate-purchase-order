import {
  dotToComma,
  currencyToNumber,
  getRentabilidade
} from "../functions";

describe("dotToComma", () => {
  it("should format string with dot to string with comma", () => {
    const stringWithComma = dotToComma('1234.33');
    expect(stringWithComma).toEqual('1234,33');
  });
});

describe("currencyToNumber", () => {
  it("should format string in number", () => {
    const number = currencyToNumber("R$ 1.2..3.4,33");
    expect(number).toEqual(1234.33);
  });
});

describe("getRentabilidade", () => {
  it("should return Ótima", () => {
    const rentabilidade = getRentabilidade("R$ 1000,00", "R$ 2000,00");
    expect(rentabilidade).toEqual("Ótima");
  });

  it("should return Boa", () => {
    const rentabilidade = getRentabilidade("R$ 1000,00", "R$ 900,00");
    expect(rentabilidade).toEqual("Boa");
  });

  it("should return Ruim", () => {
    const rentabilidade = getRentabilidade("R$ 1000,00", "R$ 899,99");
    expect(rentabilidade).toEqual("Ruim");
  });
});
