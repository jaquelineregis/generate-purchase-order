export const dotToComma = stringWithDot => {
  return stringWithDot.replace(".", ",");
};

export const currencyToNumber = currency => {
  return Number(
    currency
      .replace("R$ ", "")
      .replace(/\./g, "")
      .replace(",", ".")
  );
};

export const getRentabilidade = (precoInicial, precoFinal) => {
  const originalPreco = currencyToNumber(precoInicial);
  const finalPreco = currencyToNumber(precoFinal);

  if (finalPreco > originalPreco) {
    return "Ótima";
  } else if (finalPreco <= originalPreco && finalPreco >= originalPreco * 0.9) {
    return "Boa";
  }
  return "Ruim";
};
