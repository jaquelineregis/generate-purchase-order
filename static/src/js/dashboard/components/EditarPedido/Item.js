import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'react-bootstrap';

import Produtos from './Produtos';
import Quantidade from './Quantidade';
import Preco from './Preco';
import Rentabilidade from './Rentabilidade';

export class Item extends Component {
  static propTypes = {
    item: PropTypes.object
  }

  render() {
    return (
      <tr>
        <td />
        <td>
          <Produtos />
        </td>
        <td>
          <Quantidade />
        </td>
        <td>
          <Preco />
        </td>
        <td>
          <Rentabilidade />
        </td>
        <td>
          <Button variant="danger">
            X
          </Button>
        </td>
      </tr>
    );
  }
}

export default Item;
