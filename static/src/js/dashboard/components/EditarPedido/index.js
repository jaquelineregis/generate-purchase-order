import React from "react";
import { Form, Button } from "react-bootstrap";

import Clientes from "./Clientes";
import Itens from "./Itens";

export default function EditarPedido() {
  return (
    <Form>
      <Clientes />
      <Itens />
      <Button type="submit" variant="primary">
        Alterar Pedido
      </Button>
      <Button type="submit" variant="danger">
        Excluir Pedido
      </Button>
    </Form>
  );
}
