import React from 'react'
import { Form } from 'react-bootstrap';

export default function Quantidade() {
  return (
    <Form.Group>
      <Form.Control type="number" name="quantidade" />
    </Form.Group>
  );
}
