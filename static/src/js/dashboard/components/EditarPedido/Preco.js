import React from 'react'
import { Form } from 'react-bootstrap';

export default function Preco() {
  return (
    <Form.Group>
      <Form.Control type="number" name="preco" />
    </Form.Group>
  );
}
