import React from 'react'
import { Form } from 'react-bootstrap';

export default function Produtos() {
  return (
    <Form.Group>
      <Form.Control name="produto" as="select">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </Form.Control>
    </Form.Group>
  );
}
