import React from 'react';
import { Form } from 'react-bootstrap';

function Clientes() {
  return (
    <Form.Group>
      <Form.Label>Cliente</Form.Label>
      <Form.Control name="cliente" as="select">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
      </Form.Control>
    </Form.Group>
  );
}

export default Clientes;
