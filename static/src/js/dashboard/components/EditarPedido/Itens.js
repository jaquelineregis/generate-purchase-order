import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Table, Button } from "react-bootstrap";

import Item from "./Item";

export class Itens extends Component {
  static propTypes = {
    itens: PropTypes.array
  };

  render() {
    return (
      <Table responsive>
        <thead>
          <tr>
            <th>
              <Button variant="success">+</Button>
            </th>
            <th>Produtos</th>
            <th>Quantidade</th>
            <th>Preço unitário</th>
            <th>Rentabilidade</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          {this.props.itens.map((item, index) => (
            <Item key={index} item={item} />
          ))}
          {this.props.itens.length === 0 && (
            <tr>
              <td colSpan={6}>Adicione um item!</td>
            </tr>
          )}
        </tbody>
      </Table>
    );
  }
}

const mapStateToProps = state => ({
  itens: state.pedido.itens
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Itens);
