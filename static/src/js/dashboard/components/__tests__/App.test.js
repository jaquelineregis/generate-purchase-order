import React from 'react';
import { shallow } from 'enzyme';

import App from '../App';
import Routes from '../Routes';
import Layout from '../Layout';

describe('App component', () => {
  it('should render with Header and Routes', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find(Layout)).toHaveLength(1);
    expect(wrapper.find(Routes)).toHaveLength(1);
  });
});
