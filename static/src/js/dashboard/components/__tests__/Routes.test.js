import React from 'react';
import { shallow } from 'enzyme';

import Routes from '../Routes';
import RealizarPedido from '../RealizarPedido';
import ListarPedidos from '../ListarPedidos';
import EditarPedido from '../EditarPedido';

describe('Routes component', () => {
  it('should find route / to RealizarPedido', () => {
    const wrapper = shallow(<Routes />);
    expect(wrapper.find('Route[exact=true][path="/"]').props().component).toBe(RealizarPedido);
  });

  it('should find route /listar/ to ListarPedidos', () => {
    const wrapper = shallow(<Routes />);
    expect(wrapper.find('Route[exact=true][path="/listar/"]').props().component).toBe(ListarPedidos);
  });

  it('should find route /editar/:pedidoId/ to EditarPedido', () => {
    const wrapper = shallow(<Routes />);
    expect(wrapper.find('Route[exact=true][path="/editar/:pedidoId/"]').props().component).toBe(EditarPedido);
  });
});
