import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import Layout from './Layout'
import Routes from './Routes';

export class App extends Component {
  render() {
    return (
      <>
        <BrowserRouter>
          <Container>
            <Layout>
              <Routes />
            </Layout>
          </Container>
        </BrowserRouter>
      </>
    );
  }
}

export default App;
