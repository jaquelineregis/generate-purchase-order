import React, { Component } from 'react';
import { Route } from 'react-router-dom';

import RealizarPedido from './RealizarPedido';
import ListarPedidos from './ListarPedidos';
import EditarPedido from './EditarPedido';

export class Routes extends Component {
  render() {
    return (
      <div>
        <Route exact path="/" component={RealizarPedido} />
        <Route exact path="/listar/" component={ListarPedidos} />
        <Route exact path="/editar/:pedidoId/" component={EditarPedido} />
      </div>
    );
  }
}

export default Routes;
