import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Form, Button } from "react-bootstrap";

import { getClientes } from "../../actions/clientesActions";
import { getProdutos } from "../../actions/produtosActions";
import Clientes from "./Clientes";
import Itens from "./Itens";

export class RealizarPedido extends Component {
  static propTypes = {
    getClientes: PropTypes.func.isRequired,
    getProdutos: PropTypes.func.isRequired,
    itens: PropTypes.array,
    clientes: PropTypes.array
  };

  componentDidMount() {
    this.props.getClientes();
    this.props.getProdutos();
  }

  render() {
    return (
      <Form>
        <Clientes clientes={this.props.clientes} />
        <Itens />
        <Button
          type="submit"
          variant="primary"
          disabled={this.props.itens.length === 0}
        >
          Finalizar Pedido
        </Button>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  itens: state.pedido.itens,
  clientes: state.clientes.clientes
});

const mapDispatchToProps = {
  getProdutos,
  getClientes
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RealizarPedido);
