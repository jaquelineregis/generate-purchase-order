import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";
import { addCliente } from "../../actions/pedidoActions";

export class Clientes extends Component {
  static propTypes = {
    clientes: PropTypes.array
  };

  componentDidUpdate(){
    this.props.addCliente(this.props.clientes[0]);
  }

  handleChange = evt => {
    const cliente = this.props.clientes.filter(
      cliente => cliente.id === Number(evt.target.value)
    )[0];
    this.props.addCliente(cliente);
  };

  render() {
    return (
      <Form.Group>
        <Form.Label>Cliente</Form.Label>
        <Form.Control name="cliente" as="select" onChange={this.handleChange}>
          {this.props.clientes.map(cliente => (
            <option key={cliente.id} value={cliente.id}>
              {cliente.nome}
            </option>
          ))}
        </Form.Control>
      </Form.Group>
    );
  }
}

const mapStateToProps = state => ({
  clientes: state.clientes.clientes
});

const mapDispatchToProps = {
  addCliente
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Clientes);
