import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";

import { updateQuantidade } from "../../actions/pedidoActions";

export class Quantidade extends Component {
  static propTypes = {
    index: PropTypes.number,
    quantidade: PropTypes.number,
    multiplo: PropTypes.number,
    updateQuantidade: PropTypes.func
  };

  state = {
    quantidade: this.props.quantidade
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.quantidade !== this.props.quantidade) {
      this.setState({
        quantidade: nextProps.quantidade
      });
    }
  }

  handleChange = evt => {
    this.setState({
      quantidade: evt.target.value
    });
  };

  handleBlur = evt => {
    this.props.updateQuantidade(this.props.index, evt.target.value);
  };

  render() {
    return (
      <Form.Group>
        <Form.Control
          type="number"
          value={this.state.quantidade}
          min={this.props.multiplo}
          step={this.props.multiplo}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
        />
      </Form.Group>
    );
  }
}

export default connect(
  null,
  { updateQuantidade }
)(Quantidade);
