import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'react-bootstrap';

export class Produtos extends Component {
  static propTypes = {
    produtos: PropTypes.array,
    selectProduto: PropTypes.func
  }

  handleChange = evt => {
    const produto = this.props.produtos.filter(produto => produto.id === Number(evt.target.value))[0];
    this.props.selectProduto(produto);
  };

  render() {
    return (
      <Form.Group>
        <Form.Control name="produto" as="select" onChange={this.handleChange}>
          {this.props.produtos.map(produto => (
            <option
              key={produto.id}
              value={produto.id}
            >
              {produto.nome}
            </option>
          ))}
        </Form.Control>
      </Form.Group>
    )
  }
}

export default Produtos;
