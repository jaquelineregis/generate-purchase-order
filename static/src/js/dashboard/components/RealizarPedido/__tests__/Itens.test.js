import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import { Button } from 'react-bootstrap';

import { Itens } from '../Itens';
import Item from '../Item';

describe('Itens component', () => {
  it('should render a table with itens empty and warning phrase', () => {
    const props = {
      itens: [],
    }
    const tree = renderer.create(<Itens {...props} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render a table with two Item component', () => {
    const props = {
      itens: [{}, {}],
    }
    const wrapper = shallow(<Itens {...props} />);
    expect(wrapper.find(Item)).toHaveLength(2);
  });

  it('should called `addItem` when on click button and get the first produto', () => {
    const props = {
      itens: [{}, {}],
      produtos: [
        { nome: 'Millenium​ ​Falcon', multiplo: 2, preco: '550000.00' },
        { fake: 'fake' },
      ],
      addItem: jest.fn(),
    }
    const wrapper = shallow(<Itens {...props} />);
    wrapper.find(Button).simulate('click');
    expect(props.addItem).toHaveBeenCalledWith(props.produtos[0]);
  });
});
