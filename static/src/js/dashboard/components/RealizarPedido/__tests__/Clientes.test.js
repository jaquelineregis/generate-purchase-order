import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { Form } from 'react-bootstrap';

import { Clientes } from '../Clientes';

describe('Clientes component', () => {
  let props = {
    clientes: [
      { id: 1, nome: 'Darth​ ​Vader' },
      { id: 2, nome: 'Luke​ ​Skywalker' },
    ],
    addCliente: jest.fn()
  };

  it('should render a select with two options', () => {
    const tree = renderer.create(<Clientes {...props} />);
    expect(tree).toMatchSnapshot()
  });

  it('should called `componentDidUpdate`', () => {
    const wrapper = shallow(<Clientes {...props} />);
    wrapper.instance().componentDidUpdate();
    expect(props.addCliente).toHaveBeenCalledWith(props.clientes[0]);
  });

  it("should call `addCliente` when on change", () => {
    const wrapper = shallow(<Clientes {...props} />);
    wrapper.find(Form.Control).simulate("change", { target: { value: '2' } });
    expect(props.addCliente).toHaveBeenCalledWith(props.clientes[1]);
  });
});
