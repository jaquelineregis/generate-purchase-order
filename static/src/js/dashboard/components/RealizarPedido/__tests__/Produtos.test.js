import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import { Form } from 'react-bootstrap';

import { Produtos } from '../Produtos';

describe('Produtos component', () => {
  let props = {
    selectProduto: jest.fn(),
    produtos: [
      { id: 1, nome: 'Millenium​ ​Falcon' },
      { id: 2, nome: 'X-Wing' },
    ],
  };

  it('should render a select with two options', () => {
    const tree = renderer.create(<Produtos {...props} />);
    expect(tree).toMatchSnapshot()
  });

  it("should call `selectProduto` when on change", () => {
    const wrapper = shallow(<Produtos {...props} />);
    wrapper.find(Form.Control).simulate("change", { target: { value: '2' } });
    expect(props.selectProduto).toHaveBeenCalledWith(props.produtos[1]);
  });
});
