import React from 'react';
import { shallow } from 'enzyme';
import { Button } from 'react-bootstrap';

import { RealizarPedido } from '../index';
import Clientes from '../Clientes';
import Itens from '../Itens';

describe('RealizarPedido component', () => {
  let props;
  let wrapper;

  beforeEach(() => {
    props = {
      getClientes: jest.fn(),
      getProdutos: jest.fn(),
      itens: []
    };
    wrapper = shallow(<RealizarPedido {...props} />);
  });

  it('should called `componentDidMount`', () => {
    wrapper.instance().componentDidMount();
    expect(props.getClientes).toHaveBeenCalledWith();
    expect(props.getProdutos).toHaveBeenCalledWith();
  });

  it('should render Clientes component ', () => {
    expect(wrapper.find(Clientes)).toHaveLength(1);
  });

  it('should render Itens component ', () => {
    expect(wrapper.find(Itens)).toHaveLength(1);
  });

  it('should render Button disabled if itens empty ', () => {
    expect(wrapper.find(Button).prop('disabled')).toEqual(true);
  });

  it('should not disabled button if itens not empty ', () => {
    props.itens = [{}, {}];
    wrapper = shallow(<RealizarPedido {...props}/>);
    expect(wrapper.find(Button).prop('disabled')).toEqual(false);
  });
});
