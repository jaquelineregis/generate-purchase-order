import React from "react";
import { shallow } from "enzyme";
import { Form } from "react-bootstrap";

import { Quantidade } from "../Quantidade";

describe("<Quantidade />", () => {
  let props;
  let wrapper;

  it("should change the quantidade in state when on change", () => {
    props = { quantidade: 2, multiplo: 2 };
    wrapper = shallow(<Quantidade {...props} />);
    wrapper.find(Form.Control).simulate("change", { target: { value: 4 } });
    expect(wrapper.find(Form.Control).props().value).toEqual(4);
 });

 it("should render value equal quantidade and min, step equals multiplo", () => {
    props = { quantidade: 1, multiplo: 2 };
    wrapper = shallow(<Quantidade {...props} />);
    expect(wrapper.find(Form.Control).props().value).toEqual(1);
    expect(wrapper.find(Form.Control).props().min).toEqual(2);
    expect(wrapper.find(Form.Control).props().step).toEqual(2);
 });

 it("should call `updateQuantidade` and change the quantidade in state when on blur", () => {
    props = {
        index: 1,
        quantidade: 2,
        updateQuantidade: jest.fn()
    };
    wrapper = shallow(<Quantidade {...props} />);
    wrapper.find(Form.Control).simulate("blur", { target: { value: 7 } });
    expect(props.updateQuantidade).toHaveBeenCalledWith(props.index, 7);
  });

  it("should call `componentWillReceiveProps` change state quantidade", () => {
    props = { quantidade: 4 };
    wrapper = shallow(<Quantidade {...props} />);
    expect(wrapper.state().quantidade).toEqual(props.quantidade);
    wrapper.setProps({ quantidade: 6 });
    expect(wrapper.state().quantidade).toEqual(6);
  });
});

