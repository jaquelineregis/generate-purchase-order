import React from "react";
import renderer from "react-test-renderer";

import Rentabilidade from "../Rentabilidade";

describe("<Rentabilidade />", () => {
  it("should render a Badge with text Ótima", () => {
    const wrapper = renderer.create(<Rentabilidade rentabilidade="Ótima" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("should render a Badge with text Ruim", () => {
    const wrapper = renderer.create(<Rentabilidade rentabilidade="Ruim" />);
    expect(wrapper).toMatchSnapshot();
  });

  it("should render a Badge with text Boa", () => {
    const wrapper = renderer.create(<Rentabilidade rentabilidade="Boa" />);
    expect(wrapper).toMatchSnapshot();
  });
});
