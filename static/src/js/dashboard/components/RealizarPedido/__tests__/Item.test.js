import React from "react";
import { shallow } from "enzyme";
import { Button } from "react-bootstrap";

import { Item } from "../Item";
import Produtos from "../Produtos";
import Quantidade from "../Quantidade";
import Preco from "../Preco";
import Rentabilidade from "../Rentabilidade";

describe("Item component", () => {
  let props;
  let wrapper;

  beforeEach(() => {
    props = {
      index: 1,
      deleteItem: jest.fn(),
      updatePrecoFinalAndRentabilidade: jest.fn(),
      updateItem: jest.fn(),
      item: {
        precoInicial: "1234,56",
        precoFinal: "124,56",
        rentabilidade: "Ruim"
      },
      produtos: []
    };
    wrapper = shallow(<Item {...props} />);
  });

  it("should render Produtos component", () => {
    expect(wrapper.find(Produtos)).toHaveLength(1);
  });

  it("should render Quantidade component", () => {
    expect(wrapper.find(Quantidade)).toHaveLength(1);
  });

  it("should render Preco component", () => {
    expect(wrapper.find(Preco)).toHaveLength(1);
  });

  it("should render Rentabilidade component", () => {
    expect(wrapper.find(Rentabilidade)).toHaveLength(1);
  });

  it("should call `deleteItem` when click on button", () => {
    wrapper.find(Button).simulate("click");
    expect(props.deleteItem).toHaveBeenCalledWith(props.index);
  });

  it("should change initial rentabilidade when call `getNewPrecoFinal`", () => {
    wrapper.instance().getNewPrecoFinal("R$ 100.000,00");
    expect(wrapper.state().rentabilidade).toEqual("Ótima");
  });

  it("should call `updatePrecoFinalAndRentabilidade` when call `getNewPrecoFinal`", () => {
    wrapper.instance().getNewPrecoFinal("R$ 100,00");
    expect(props.updatePrecoFinalAndRentabilidade).toHaveBeenCalledWith(
      props.index,
      "R$ 100,00",
      wrapper.state().rentabilidade
    );
  });

  it("should call `updateItem` when call `getProduto`", () => {
    const produto = { id: 1, nome: 'Millenium​ ​Falcon', multiplo: 2, preco: '1234.50' }
    wrapper.instance().getProduto(produto);
    expect(props.updateItem).toHaveBeenCalledWith(
      props.index,
      { id: 1, nome: 'Millenium​ ​Falcon', multiplo: 2, preco: '1234.50' },
    );
  });

  it("should change initial state when call `getProduto`", () => {
    const produto = { id: 1, nome: 'Millenium​ ​Falcon', multiplo: 2, preco: '4321.50' }
    wrapper.instance().getProduto(produto);
    expect(wrapper.state().precoInicial).toEqual('4321,50');
    expect(wrapper.state().rentabilidade).toEqual("Boa");
    expect(wrapper.state().quantidade).toEqual(produto.multiplo);
    expect(wrapper.state().multiplo).toEqual(produto.multiplo);
  });
});
