import React from "react";
import { shallow } from "enzyme";

import Preco from "../Preco";

describe("<Preco />", () => {
  let props;
  let wrapper;

  it("should call `getNewPrecoFinal` and change the preco in state when on change", () => {
    props = {
      precoInicial: "123,45",
      getNewPrecoFinal: jest.fn()
    };
    wrapper = shallow(<Preco {...props} />);
    wrapper.simulate("change", { target: { value: "456,23" } });
    expect(wrapper.props().value).toEqual("456,23");
    expect(props.getNewPrecoFinal).toHaveBeenCalledWith("456,23");
  });

  it("should call `componentWillReceiveProps` change state preco", () => {
    props = { precoInicial: "123,45" };
    wrapper = shallow(<Preco {...props} />);
    expect(wrapper.state().preco).toEqual(props.precoInicial);
    wrapper.setProps({ precoInicial: "321,32" });
    expect(wrapper.state().preco).toEqual("321,32");
  });
});
