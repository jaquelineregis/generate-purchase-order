import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Button } from "react-bootstrap";

import Produtos from "./Produtos";
import Quantidade from "./Quantidade";
import Preco from "./Preco";
import Rentabilidade from "./Rentabilidade";
import {
  deleteItem,
  updatePrecoFinalAndRentabilidade,
  updateItem
} from "../../actions/pedidoActions";
import { getRentabilidade, dotToComma } from "../../utils/functions";

export class Item extends Component {
  static propTypes = {
    index: PropTypes.number,
    item: PropTypes.shape({
      rentabilidade: PropTypes.string,
      precoInicial: PropTypes.string
    }),
    produtos: PropTypes.array,
    deleteItem: PropTypes.func,
    updatePrecoFinalAndRentabilidade: PropTypes.func,
    updateItem: PropTypes.func
  };

  state = {
    precoInicial: this.props.item.precoInicial,
    rentabilidade: this.props.item.rentabilidade,
    quantidade: this.props.item.quantidade,
    multiplo: this.props.item.multiplo
  };

  getNewPrecoFinal = newPreco => {
    const rentabilidade = getRentabilidade(
      this.state.precoInicial,
      newPreco
    );
    this.props.updatePrecoFinalAndRentabilidade(
      this.props.index,
      newPreco,
      rentabilidade
    );
    this.setState({ rentabilidade });
  };

  getProduto = produto => {
    this.props.updateItem(this.props.index, produto);
    this.setState({
      precoInicial: dotToComma(produto.preco),
      rentabilidade: "Boa",
      quantidade: produto.multiplo,
      multiplo: produto.multiplo
    });
  };

  render() {
    return (
      <tr>
        <td />
        <td>
          <Produtos
            produtos={this.props.produtos}
            selectProduto={this.getProduto}
          />
        </td>
        <td>
          <Quantidade
            index={this.props.index}
            quantidade={this.state.quantidade}
            multiplo={this.state.multiplo}
          />
        </td>
        <td>
          <Preco
            precoInicial={this.state.precoInicial}
            getNewPrecoFinal={this.getNewPrecoFinal}
          />
        </td>
        <td>
          <Rentabilidade rentabilidade={this.state.rentabilidade} />
        </td>
        <td>
          <Button
            variant="danger"
            onClick={() => this.props.deleteItem(this.props.index)}
          >
            X
          </Button>
        </td>
      </tr>
    );
  }
}

export default connect(
  null,
  {
    deleteItem,
    updatePrecoFinalAndRentabilidade,
    updateItem
  }
)(Item);
