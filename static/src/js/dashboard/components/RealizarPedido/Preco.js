import React, { Component } from "react";
import PropTypes from "prop-types";
import MaskedInput from "react-text-mask";
import createNumberMask from "text-mask-addons/dist/createNumberMask";

export default class Preco extends Component {
  static propTypes = {
    getNewPrecoFinal: PropTypes.func,
    precoInicial: PropTypes.string
  };

  state = {
    preco: this.props.precoInicial
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.precoInicial !== this.props.precoInicial) {
      this.setState({
        preco: nextProps.precoInicial
      })
    }
  }

  handleChange = evt => {
    this.setState({
      preco: evt.target.value
    });
    this.props.getNewPrecoFinal(evt.target.value);
  };

  render() {
    const numberMask = createNumberMask({
      prefix: "R$ ",
      thousandsSeparatorSymbol: ".",
      decimalSymbol: ",",
      allowDecimal: true
    });

    return (
      <MaskedInput
        mask={numberMask}
        value={this.state.preco}
        className="form-control"
        onChange={this.handleChange}
      />
    );
  }
}
