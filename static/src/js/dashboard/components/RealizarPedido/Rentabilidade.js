import React from 'react';
import PropTypes from 'prop-types'
import { Badge } from 'react-bootstrap';

export default function Rentabilidade(props) {
  return (
    <>
      {props.rentabilidade === "Ótima" &&
        <Badge variant="success">Ótima</Badge>
      }

      {props.rentabilidade === "Boa" &&
        <Badge variant="primary">Boa</Badge>
      }

      {props.rentabilidade === "Ruim" &&
        <Badge variant="danger">Ruim</Badge>
      }
    </>
  );
}

Rentabilidade.propTypes = {
  rentabilidade: PropTypes.string
};
