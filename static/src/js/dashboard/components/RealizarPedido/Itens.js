import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Table, Button } from "react-bootstrap";

import Item from "./Item";
import { addItem } from "../../actions/pedidoActions";

export class Itens extends Component {
  static propTypes = {
    itens: PropTypes.array,
    produtos: PropTypes.array,
    addItem: PropTypes.func
  };

  handleClick = () => {
    this.props.addItem(this.props.produtos[0]);
  };

  render() {
    return (
      <Table responsive>
        <thead>
          <tr>
            <th>
              <Button variant="success" onClick={this.handleClick}>
                +
              </Button>
            </th>
            <th>Produtos</th>
            <th>Quantidade</th>
            <th>Preço unitário</th>
            <th>Rentabilidade</th>
            <th>Ações</th>
          </tr>
        </thead>
        <tbody>
          {this.props.itens.map((item, index) => (
            <Item
              key={index}
              item={item}
              index={index}
              produtos={this.props.produtos}
            />
          ))}
          {this.props.itens.length === 0 && (
            <tr>
              <td colSpan={6}>Adicione um item!</td>
            </tr>
          )}
        </tbody>
      </Table>
    );
  }
}

const mapStateToProps = state => ({
  itens: state.pedido.itens,
  produtos: state.produtos.produtos
});

export default connect(
  mapStateToProps,
  { addItem }
)(Itens);
