import React from 'react';
import renderer from 'react-test-renderer';

import Layout from '../index';

describe('Layout component', () => {
  it('should render a Navbar Container and hr and message', () => {
    const tree = renderer.create(<Layout>Hello!</Layout>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
