import React from 'react'
import { Navbar, Nav } from 'react-bootstrap'

export default function Header() {
  return (
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand href="/">Emissão de Pedidos</Navbar.Brand>
      <Nav className="mr-auto">
        <Nav.Link href="/">Realizar Pedido</Nav.Link>
        <Nav.Link href="/listar/">Listar Pedidos</Nav.Link>
      </Nav>
    </Navbar>
  );
}

