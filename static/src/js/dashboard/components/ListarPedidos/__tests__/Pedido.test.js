import React from 'react'
import { shallow } from 'enzyme';

import { Pedido } from '../Pedido'
import { Collapse } from 'react-bootstrap';

describe('<Pedido />', () => {
  it('should render default close collapse when click detalhar button then open collapse', () => {
    const wrapper = shallow(<Pedido />);
    expect(wrapper.find(Collapse).props().in).toBe(false);
    wrapper.find('#detalhar').simulate('click');
    expect(wrapper.find(Collapse).props().in).toBe(true);
  });

  it('should render open collapse when click detalhar button then close collapse', () => {
    const wrapper = shallow(<Pedido />);
    wrapper.setState({ open: true });
    wrapper.find('#detalhar').simulate('click');
    expect(wrapper.find(Collapse).props().in).toBe(false);
  });
})

