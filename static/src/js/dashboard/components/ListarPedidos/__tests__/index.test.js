import React from 'react';
import renderer from 'react-test-renderer';

import ListarPedidos from '../index';

describe('ListarPedidos component', () => {
  it('should render a Table with fake data', () => {
    const tree = renderer.create(<ListarPedidos />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
