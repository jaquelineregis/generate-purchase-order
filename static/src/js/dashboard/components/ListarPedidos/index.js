import React from 'react'
import { Table } from 'react-bootstrap';

import Pedido from './Pedido';

export default function ListarPedidos() {
  return (
    <Table responsive>
      <thead>
        <tr>
          <th>Cliente</th>
          <th>Pedido</th>
          <th>Data</th>
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
        <Pedido />
      </tbody>
    </Table>
  );
}
