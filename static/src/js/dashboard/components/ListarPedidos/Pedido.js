import React, { Component } from 'react'
import { Button, Collapse, Table } from 'react-bootstrap';

export class Pedido extends Component {
  state = {
    open: false,
  }

  toggleOpen = () => {
    this.setState({
      open: !this.state.open,
    });
  }

  render() {
    return (
      <>
      <tr>
        <td>Luke​ ​Skywalker</td>
        <td>P-1034</td>
        <td>21/02/2019</td>
        <td>
          <Button id="detalhar" variant="primary" onClick={this.toggleOpen}>Detalhar</Button>
          <Button href="/editar/1/" variant="success">Editar</Button>
        </td>
      </tr>
      <tr>
        <td colSpan={4}>
          <Collapse in={this.state.open}>
            <Table responsive>
              <thead>
                <tr>
                  <th>Produto</th>
                  <th>Quantidade</th>
                  <th>Valor Unitário</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>X-Wing</td>
                  <td>2</td>
                  <td>R$ 123.456,00</td>
                </tr>
              </tbody>
            </Table>
          </Collapse>
        </td>
      </tr>
    </>
    )
  }
}

export default Pedido;
