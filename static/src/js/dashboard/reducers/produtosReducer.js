import types from '../actions/types';

const initialState = {
  produtos: [],
}

export default (state = initialState, { type, payload }) => {
  switch (type) {

    case types.GET_PRODUTOS:
      return {
        ...state,
        produtos: payload,
      }

    default:
      return state
  }
}

