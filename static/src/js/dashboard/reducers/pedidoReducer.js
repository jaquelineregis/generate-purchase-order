import types from "../actions/types";
import { dotToComma } from "../utils/functions";

const initialState = {
  cliente: null,
  itens: []
};

const produtoToItem = payload => {
  return {
    id: payload.produto.id,
    nome: payload.produto.nome,
    quantidade: payload.produto.multiplo,
    multiplo: payload.produto.multiplo,
    precoInicial: dotToComma(payload.produto.preco),
    precoFinal: dotToComma(payload.produto.preco),
    rentabilidade: "Boa"
  };
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.ADD_CLIENTE:
      return {
        ...state,
        cliente: payload.cliente
      };

    case types.ADD_ITEM:
      return {
        ...state,
        itens: state.itens.concat(produtoToItem(payload))
      };

    case types.DELETE_ITEM:
      return {
        ...state,
        itens: state.itens.filter((_, index) => index !== payload)
      };

    case types.UPDATE_ITEM:
      state.itens[payload.index] = produtoToItem(payload);
      return state;

    case types.UPDATE_PRECO_FINAL_AND_RENTABILIDADE_ITEM:
      state.itens[payload.index].precoFinal = payload.newPreco;
      state.itens[payload.index].rentabilidade = payload.rentabilidade;
      return state;

    case types.UPDATE_QUANTIDADE_IN_ITEM:
      state.itens[payload.index].quantidade = Number(payload.quantidade);
      return state;

    default:
      return state;
  }
};
