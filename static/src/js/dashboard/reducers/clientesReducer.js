import types from '../actions/types';

const initialState = {
  clientes: [],
};

export default function (state = initialState, { type, payload }) {
  switch (type) {

    case types.GET_CLIENTES:
      return {
        ...state,
        clientes: payload,
      }

    default:
      return state;
  }
};
