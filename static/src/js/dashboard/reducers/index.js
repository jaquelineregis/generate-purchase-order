import { combineReducers } from 'redux';

import pedido from './pedidoReducer'
import produtos from './produtosReducer'
import clientes from './clientesReducer'

export default combineReducers({
  pedido,
  produtos,
  clientes,
});
