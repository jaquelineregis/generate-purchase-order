import reducer from "../pedidoReducer";
import types from "../../actions/types";

let initialState = {
  cliente: null,
  itens: []
};

describe("itens reducer", () => {
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it("should handle ADD_ITEM", () => {
    const produto = {
      nome: "Millenium​ ​Falcon",
      multiplo: 2,
      preco: "550000.00"
    };
    const action = {
      type: types.ADD_ITEM,
      payload: { produto }
    };
    const correctState = {
      cliente: null,
      itens: [
        {
          multiplo: 2,
          nome: "Millenium​ ​Falcon",
          precoFinal: "550000,00",
          precoInicial: "550000,00",
          quantidade: 2,
          rentabilidade: "Boa"
        }
      ]
    };
    const newState = reducer(initialState, action);
    expect(newState).toEqual(correctState);
  });

  it("should handle DELETE_ITEM", () => {
    const action = {
      type: types.DELETE_ITEM,
      payload: 1
    };
    initialState = {
      cliente: null,
      itens: [{ preco: 1 }, { preco: 2 }, { preco: 3 }]
    };
    const correctState = {
      cliente: null,
      itens: [{ preco: 1 }, { preco: 3 }]
    };
    const newState = reducer(initialState, action);
    expect(newState).toEqual(correctState);
  });

  it("should handle UPDATE_ITEM", () => {
    const index = 1;
    const produto = {
      nome: "DL-44​ ​Heavy​ ​Blaster​ ​Pistol",
      multiplo: 10,
      preco: "1500.00"
    };
    const action = {
      type: types.UPDATE_ITEM,
      payload: { index, produto }
    };
    initialState = {
      cliente: null,
      itens: [
        {
          multiplo: 1,
          nome: "Millenium​ ​Falcon",
          precoFinal: "550000,00",
          precoInicial: "550000,00",
          quantidade: 1,
          rentabilidade: "Boa"
        },
        {
          multiplo: 2,
          nome: "X-Wing",
          precoFinal: "60000,00",
          precoInicial: "60000,00",
          quantidade: 2,
          rentabilidade: "Boa"
        }
      ]
    };
    const correctState = {
      cliente: null,
      itens: [
        {
          multiplo: 1,
          nome: "Millenium​ ​Falcon",
          precoFinal: "550000,00",
          precoInicial: "550000,00",
          quantidade: 1,
          rentabilidade: "Boa"
        },
        {
          multiplo: 10,
          nome: "DL-44​ ​Heavy​ ​Blaster​ ​Pistol",
          precoFinal: "1500,00",
          precoInicial: "1500,00",
          quantidade: 10,
          rentabilidade: "Boa"
        }
      ]
    };
    const newState = reducer(initialState, action);
    expect(newState).toEqual(correctState);
  });

  it("should handle UPDATE_PRECO_FINAL_AND_RENTABILIDADE_ITEM", () => {
    const action = {
      type: types.UPDATE_PRECO_FINAL_AND_RENTABILIDADE_ITEM,
      payload: { index: 1, newPreco: "123,45", rentabilidade: "Ruim" }
    };
    initialState = {
      cliente: null,
      itens: [
        { precoFinal: "1,0", rentabilidade: "Boa" },
        { precoFinal: "1,0", rentabilidade: "Boa" },
        { precoFinal: "1,0", rentabilidade: "Boa" }
      ]
    };
    const correctState = {
      cliente: null,
      itens: [
        { precoFinal: "1,0", rentabilidade: "Boa" },
        { precoFinal: "123,45", rentabilidade: "Ruim" },
        { precoFinal: "1,0", rentabilidade: "Boa" }
      ]
    };
    const newState = reducer(initialState, action);
    expect(newState).toEqual(correctState);
  });

  it('should handle UPDATE_QUANTIDADE_IN_ITEM', () => {
    const action = {
      type: types.UPDATE_QUANTIDADE_IN_ITEM,
      payload: { index: 1, quantidade: '6' },
    };
    initialState = {
      cliente: null,
      itens: [{ quantidade: 1 }, { quantidade: 2 }, { quantidade: 3 }]
    }
    const correctState = {
      cliente: null,
      itens: [{ quantidade: 1 }, { quantidade: 6 }, { quantidade: 3 }]
    }
    const newState = reducer(initialState, action);
    expect(newState).toEqual(correctState);
  });

  it("should handle unknown type", () => {
    const newState = reducer(initialState, { type: "UNKNOWN_TYPE" });
    expect(newState).toEqual(initialState);
  });
});
