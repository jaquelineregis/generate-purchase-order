import reducer from '../clientesReducer';
import types from '../../actions/types';

const initialState = {
  clientes: []
}

describe('clientes reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle GET_CLIENTES', () => {
    const data = {
      clientes: [
        { id: 1, nome: 'Darth​ ​Vader' },
        { id: 2, nome: 'Luke​ ​Skywalker' },
      ],
    };
    const action = {
      type: types.GET_CLIENTES,
      payload: data.clientes,
    };
    const newState = reducer(initialState, action);
    expect(newState).toEqual(data);
  });

  it('should handle unknown type', () => {
    const newState = reducer(initialState, { type: 'UNKNOWN_TYPE' });
    expect(newState).toEqual(initialState);
  });
});
