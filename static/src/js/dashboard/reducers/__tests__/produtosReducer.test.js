import reducer from '../produtosReducer';
import types from '../../actions/types';

let initialState = {
  produtos: []
}

describe('produtos reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should handle GET_PRODUTOS', () => {
    const data = {
      produtos: [
        { id: 1, nome: 'Millenium​ ​Falcon', preco: 550000.00, multiplo: 1 },
        { id: 1, nome: 'X-Wing', preco: 60000.00, multiplo: 5 },
      ],
    };
    const action = {
      type: types.GET_PRODUTOS,
      payload: data.produtos,
    };
    const newState = reducer(initialState, action);
    expect(newState).toEqual(data);
  });

  it('should handle unknown type', () => {
    const newState = reducer(initialState, { type: 'UNKNOWN_TYPE' });
    expect(newState).toEqual(initialState);
  });
});
