import {
  addItem,
  deleteItem,
  updateItem,
  updatePrecoFinalAndRentabilidade,
  updateQuantidade
} from "../pedidoActions";
import types from "../types";

describe("addItem", () => {
  it("has the correct type and payload", () => {
    const produto = {
      nome: "Millenium​ ​Falcon",
      multiplo: 2,
      preco: "550000.00"
    };
    const { type, payload } = addItem(produto);
    expect(type).toEqual(types.ADD_ITEM);
    expect(payload).toEqual({ produto });
  });
});

describe("deleteItem", () => {
  it("has the correct type and payload", () => {
    const index = 1;
    const { type, payload } = deleteItem(index);
    expect(type).toEqual(types.DELETE_ITEM);
    expect(payload).toEqual(index);
  });
});

describe("updateItem", () => {
  it("has the correct type and payload", () => {
    const index = 1;
    const produto = {
      nome: "Millenium​ ​Falcon",
      multiplo: 2,
      preco: "550000.00"
    };
    const { type, payload } = updateItem(index, produto);
    expect(type).toEqual(types.UPDATE_ITEM);
    expect(payload).toEqual({ index, produto });
  });
});

describe("updatePrecoFinalAndRentabilidade", () => {
  it("has the correct type and payload", () => {
    const { type, payload } = updatePrecoFinalAndRentabilidade(
      1,
      "123,00",
      "Boa"
    );
    expect(type).toEqual(types.UPDATE_PRECO_FINAL_AND_RENTABILIDADE_ITEM);
    expect(payload).toEqual({
      index: 1,
      newPreco: "123,00",
      rentabilidade: "Boa"
    });
  });
});

describe("updateQuantidade", () => {
  it("has the correct type and payload", () => {
    const index = 1;
    const quantidade = 101;
    const { type, payload } = updateQuantidade(index, quantidade);
    expect(type).toEqual(types.UPDATE_QUANTIDADE_IN_ITEM);
    expect(payload).toEqual({ index, quantidade });
  });
});
