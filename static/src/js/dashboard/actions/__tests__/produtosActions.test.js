import mockAxios from 'axios';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';

import { getProdutos } from '../produtosActions';
import types from '../types';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore();

describe('getProdutos', () => {
  it('should make an http request for produtos', async () => {
    const mockData = {
      produtos: [
        { id: 1, nome: 'Millenium​ ​Falcon', preco: 550000.00, multiplo: 1 },
        { id: 1, nome: 'X-Wing', preco: 60000.00, multiplo: 5 },
      ],
    };
    mockAxios.get.mockResolvedValue({ data: mockData })

    const expectedActions = [
      { type: types.GET_PRODUTOS, payload: mockData }
    ];

    await store.dispatch(getProdutos());
    expect(mockAxios.get).toHaveBeenCalledTimes(1)
    expect(store.getActions()).toEqual(expectedActions);
  });
});
