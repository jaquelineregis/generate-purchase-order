import mockAxios from 'axios';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';

import { getClientes } from '../clientesActions';
import types from '../types';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore();

describe('getClientes', () => {
  it('should make an http request for clientes', async () => {
    const mockData = {
      clientes: [
        { id: 1, nome: 'Darth​ ​Vader' },
        { id: 2, nome: 'Luke​ ​Skywalker' },
      ],
    };
    mockAxios.get.mockResolvedValue({ data: mockData })

    const expectedActions = [
      { type: types.GET_CLIENTES, payload: mockData }
    ];

    await store.dispatch(getClientes());
    expect(mockAxios.get).toHaveBeenCalledTimes(1)
    expect(store.getActions()).toEqual(expectedActions);
  });
});
