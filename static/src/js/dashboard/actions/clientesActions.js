import axios from "axios";

import types from "./types";

export const CLIENTES_ENDPOINT =
  // "http://generate-purchase-order.herokuapp.com/api/clientes/";
  "http://127.0.0.1:8000/api/clientes/";


export const getClientes = () => dispatch => {
  axios
    .get(CLIENTES_ENDPOINT)
    .then(response => {
      dispatch({
        type: types.GET_CLIENTES,
        payload: response.data
      });
    })
    .catch(error => console.log(error));
};
