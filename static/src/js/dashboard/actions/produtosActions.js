import axios from "axios";

import types from "./types";

export const PRODUTOS_ENDPOINT =
  // "http://generate-purchase-order.herokuapp.com/api/produtos/";
  "http://127.0.0.1:8000/api/produtos/";

export const getProdutos = () => dispatch => {
  axios
    .get(PRODUTOS_ENDPOINT)
    .then(response => {
      dispatch({
        type: types.GET_PRODUTOS,
        payload: response.data
      });
    })
    .catch(error => console.log(error));
};
