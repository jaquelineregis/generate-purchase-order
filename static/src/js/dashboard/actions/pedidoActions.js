import types from "./types";

export const PEDIDO_ENDPOINT =
  // "http://generate-purchase-order.herokuapp.com/api/pedidos/";
  "http://127.0.0.1:8000/api/pedidos/";

export const addCliente = cliente => {
  return {
    type: types.ADD_CLIENTE,
    payload: { cliente }
  };
};

export const addItem = produto => {
  return {
    type: types.ADD_ITEM,
    payload: { produto }
  };
};

export const deleteItem = index => {
  return {
    type: types.DELETE_ITEM,
    payload: index
  };
};

export const updateItem = (index, produto) => {
  return {
    type: types.UPDATE_ITEM,
    payload: { index, produto }
  };
};

export const updatePrecoFinalAndRentabilidade = (
  index,
  newPreco,
  rentabilidade
) => {
  return {
    type: types.UPDATE_PRECO_FINAL_AND_RENTABILIDADE_ITEM,
    payload: { index, newPreco, rentabilidade }
  };
};

export const updateQuantidade = (index, quantidade) => {
  return {
    type: types.UPDATE_QUANTIDADE_IN_ITEM,
    payload: { index, quantidade }
  };
};
